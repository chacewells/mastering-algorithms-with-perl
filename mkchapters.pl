use v5.18;

my @chapters =
qw(
introduction
basic_data_structures
advanced_data_structures
sorting
searching
sets
matrices
graphs
strings
geometric_algorithms
number_systems
number_theory
cryptography
probability
statistics
);

while (my ($no, $name) = each @chapters) {
    mkdir sprintf "ch%02d-%s", 1+$no, $name;
}
