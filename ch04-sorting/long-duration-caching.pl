# Initialize the comparison cache.

%sort_by = ();

foreach $word ( @full_list ) {
    $sort_by{ $word } =
        some_complex_time_consuming_function($word);
}

@sorted_list =
    sort
        { $sort_by{ $a } <=> $sort_by{ $b } }
        @partial_list;
