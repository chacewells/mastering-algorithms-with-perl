# MERGE SORT
# divide-and conquer
# recursively divide the array into 2 equal parts
# until only working with a 1 or 2 element section
# merge the two sections, taking the smaller
# of the respective array sections by index
# O(N log(N))
package MERGESORT;

sub mergesort {
    mergesort_recurse ($_[0], 0, $#{ $_[0] });
}

sub mergesort_recurse {
    my ( $array, $first, $last ) = @_;

    if ( $last > $first ) {
        local $^W = 0;               # Silence deep recursion warning.
        my $middle = int(( $last + $first ) / 2);

        mergesort_recurse( $array, $first,       $middle );
        mergesort_recurse( $array, $middle + 1,  $last   );
        merge( $array, $first, $middle, $last );
    }
}

my @work; # A global work array.

sub merge {
    my ( $array, $first, $middle, $last ) = @_;

    my $n = $last - $first + 1;

    # Initialize work with relevant elements from the array.
    for ( my $i = $first, my $j = 0; $i <= $last; ) {
        $work[ $j++ ] = $array->[ $i++ ];
    }

    # Now do the actual merge.  Proceed through the work array
    # and copy the elements in order back to the original array.
    # $i is the index for the merge result, $j is the index in
    # first half of the working copy, $k the index in the second half.

    $middle = int(($first + $last) / 2) if $middle > $last;

    my $n1 = $middle - $first + 1;    # The size of the 1st half.

    for ( my $i = $first, my $j = 0, my $k = $n1; $i <= $last; $i++ ) {
        $array->[ $i ] =
            $j < $n1 &&
              ( $k == $n || $work[ $j ] lt $work[ $k ] ) ?
                $work[ $j++ ] :
                $work[ $k++ ];
    }
}

# this avoids the problem of deep recursion
# by walking over the array with a working area that starts at 2 and doubles
# its size at each iteration
sub mergesort_iter ($) {
    my ( $array ) = @_;

    my $N    = @$array;
    my $Nt2  = $N * 2; # N times 2.
    my $Nm1  = $N - 1; # N minus 1.

    $#work = $Nm1;

    for ( my $size = 2; $size < $Nt2; $size *= 2 ) {
        for ( my $first = 0; $first < $N; $first += $size ) {
            my $last = $first + $size - 1;
            merge( $array,
                   $first,
                   int(($first + $last) / 2),
                   $last < $N ? $last : $Nm1 );
        }
    }
}

# HEAP SORT
# works like selection sort, except a heap is used
# to track the smallest element, rather than scanning the whole array
package HEAPSORT;
use integer;
sub heapify;

sub heapsort {
    my $array = shift;

    foreach ( my $index = 1 + @$array / 2; $index--; ) {
        heapify $array, $index;
    }

    foreach ( my $last = @$array; --$last; ) {
        @{ $array }[ 0, $last ] = @{ $array }[ $last, 0 ];
        heapify $array, 0, $last;
    }
}

sub heapify {
    my ($array, $index, $last) = @_;

    $last = @$array unless defined $last;

    my $swap = $index;
    my $high = $index * 2 + 1;

    foreach ( my $try = $index * 2;
                 $try < $last && $try <= $high;
                 $try ++ ) {
        $swap = $try if $array->[ $try ] gt $array->[ $swap ];
    }

    unless ( $swap == $index ) {
        # The heap is in disorder: must reshuffle.
        @{ $array }[ $swap, $index ] = @{ $array }[ $index, $swap ];
        heapify $array, $swap, $last;
    }
}

# QUICKSORT
# pick one element of the array and shuffle it to its final place
# pivot - the element that is moved
# partitioning - the shuffling of the element
# THE ALGORITHM IN 3 STEPS
# 1. At Point 1 (see the partition() subroutine) the elements in positions $first..$i-1 are all less than or equal to the pivot, the elements in $j+1..$last-1 are all greater than or equal to the pivot, and the element in $last is equal to the pivot.
# 2. At Point 2 the elements in $first..$i-1 are all less than or equal to the pivot, the elements in $j+1..$last-1 are all greater than or equal to the pivot, the elements in $j+1..$i-1 are all equal to the pivot, and the element at $last is equal to the pivot.
# 3. At Point 3 we have a three way partitioning. The first partition contains elements that are less than or equal to the pivot; the second partition contains elements that are all equal to the pivot. (There must be at least one of these—the original pivot element itself.) The third partition contains elements that are greater than or equal to the pivot. Only the first and third partitions need further sorting.
package QUICKSORT;
sub partition;
sub quicksort_recurse;
sub quicksort;

sub partition {
    my ( $array, $first, $last ) = @_;
    my $i = $first;
    my $j = $last - 1;
    my $pivot = $array->[ $last ];

    SCAN: {
            do {
                # $first <= $i <= $j <= $last - 1
                # Point 1.
    # Move $i as far as possible.
                while ( $array->[ $i ] le $pivot ) {
                    $i++;
                    last SCAN if $j < $i;
                }
    # Move $j as far as possible.
                while ( $array->[ $j ] ge $pivot ) {
                    $j--;
                    last SCAN if $j < $i;
                }
    # $i and $j did not cross over, so swap a low and a high value.
                @$array[ $j, $i ] = @$array[ $i, $j ];
            } while ( --$j >= ++$i );
        }
        # $first - 1 <= $j < $i <= $last
        # Point 2.
    # Swap the pivot with the first larger element (if there is one).
        if ( $i < $last ) {
            @$array[ $last, $i ] = @$array[ $i, $last ];
            ++$i;
        }
    # Point 3.
    return ( $i, $j );   # The new bounds exclude the middle.
}

sub quicksort_recurse {
    my ( $array, $first, $last ) = @_;

    if ( $last > $first ) {
        my ( $first_of_last, $last_of_first, ) =
                                partition( $array, $first, $last );

        local $^W = 0;               # Silence deep recursion warning.
        quicksort_recurse $array, $first,         $last_of_first;
        quicksort_recurse $array, $first_of_last, $last;
    }
}

sub quicksort {
    # The recursive version is bad with BIG lists
    # because the function call stack gets REALLY deep.
    quicksort_recurse $_[ 0 ], 0, $#{ $_[ 0 ] };
}

MIDDLE: { # the possbile 3rd partition
    last MIDDLE; # skip me; just here for an example
    
    { # at the end of &partition
        # Extend the middle partition as much as possible.
        ++$i while $i <= $last  && $array->[ $i ] eq $pivot;
        --$j while $j >= $first && $array->[ $j ] eq $pivot;
    }
    
    { # in &partition before `$pivot = $array->[ $last ]`
        my $middle = int( ( $first + $last ) / 2 );
        
        @$array[ $first, $middle ] = @$array[ $middle, $first ]
           if $array->[ $first ] gt $array->[ $middle ];
        
        @$array[ $first, $last ] = @$array[ $last, $first ]
           if $array->[ $first ] gt $array->[ $last ];
        
        # $array[$first] is now the smallest of the three.
        # The smaller of the other two is the middle one:
        # It should be moved to the end to be used as the pivot.
        @$array[ $middle, $last ] = @$array[ $last, $middle ]
           if $array->[ $middle ] lt $array->[ $last ];
    }

    { # choose a pivot at random (before `$pivot = $array->[ $last ]`)
        my $random = $first + rand( $last - $first + 1 );
        @$array[ $random, $last ] = @$array[ $last, $random ];
    }
}

sub quicksort_iterate {
    my ( $array, $first, $last ) = @_;
    my @stack = ( $first, $last );

    do {
        if ( $last > $first ) {
            my ( $last_of_first, $first_of_last ) =
                partition $array, $first, $last;

            # Larger first.
            if ( $first_of_last - $first > $last - $last_of_first ) {
                push @stack, $first, $first_of_last;
                $first = $last_of_first;
            } else {
                push @stack, $last_of_first, $last;
                $last = $first_of_last;
            }
        } else { 
            ( $first, $last ) = splice @stack, -2, 2;  # Double pop.
        }
    } while @stack;
}

sub quicksort_iter {
    quicksort_iterate $_[0], 0, $#{ $_[0] };
}

# finding a median (or pivot)
use constant PARTITION_SIZE => 5;

# NOTE 1: the $index in selection() is one-based, not zero-based as usual.
# NOTE 2: when $N is even, selection() returns the larger of
#         "two medians", not their average as is customary--
#         write a wrapper if this bothers you.

sub selection {
    # $array:   an array reference from which the selection is made.
    # $compare: a code reference for comparing elements,
    #           must return -1, 0, 1.
    # $index:   the wanted index in the array.
    my ($array, $compare, $index) = @_;

    my $N = @$array;
# Short circuit for partitions.
    return (sort { $compare->($a, $b) } @$array)[ $index-1 ]
         if $N <= PARTITION_SIZE;

    my $medians;

    # Find the median of the about $N/5 partitions.
    for ( my $i = 0; $i < $N; $i += PARTITION_SIZE ) {
        my $s =                 # The size of this partition.
            $i + PARTITION_SIZE < $N ?
                PARTITION_SIZE : $N - $i;
my @s =                 # This partition sorted.
            sort { $array->[ $i + $a ] cmp $array->[ $i + $b ] }
                 0 .. $s-1;
        push @{ $medians },     # Accumulate the medians.
             $array->[ $i + $s[ int( $s / 2 ) ] ];
    }

    # Recurse to find the median of the medians.
    my $median = selection( $medians, $compare, int( @$medians / 2 ) );
    my @kind;

    use constant LESS    => 0;
    use constant EQUAL   => 1;
    use constant GREATER => 2;

    # Less-than    elements end up in @{$kind[LESS]},
    # equal-to     elements end up in @{$kind[EQUAL]},
    # greater-than elements end up in @{$kind[GREATER]}.
    foreach my $elem (@$array) {
        push @{ $kind[$compare->($elem, $median) + 1] }, $elem;
    }

    return selection( $kind[LESS], $compare, $index )
        if $index <= @{ $kind[LESS]  };

    $index -= @{ $kind[LESS] };

    return $median
        if $index <= @{ $kind[EQUAL] };
 
    $index -= @{ $kind[EQUAL] };

    return selection( $kind[GREATER], $compare, $index );
}

sub median {
    my $array = shift;
    return selection( $array,
                      sub { $_[0] <=> $_[1] },
                      @$array / 2 + 1 );
}

sub percentile {
    my ($array, $percentile) = @_;
    return selection( $array,
                      sub { $_[0] <=> $_[1] },
                      (@$array * $percentile) / 100 );
}
