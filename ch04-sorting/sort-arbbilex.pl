use Sort::ArbBiLex;

*Swedish_sort = Sort::ArbBiLex::maker(
  "a A
   o O
   ä Ä
   ö Ö
  "
);
*German_sort = Sort::ArbBiLex::maker(
  "a A
   ä Ä
   o O
   ö Ö
  "
);
@words = qw(Möller Märtz Morot Mayer Mortenson Mattson);
foreach (Swedish_sort(@words)) { print "på svenska:  $_\n" }
foreach (German_sort (@words)) { print "auf Deutsch: $_\n" }