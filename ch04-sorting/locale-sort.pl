use locale;

# Fill @array here...

@dictionary_sorted =
    sort {
        my $da = lc $a;         # Translate into lowercase.
        my $db = lc $b;
        $da =~ s/[\W_]+//g;     # Remove all nonalphanumerics.
        $db =~ s/[\W_]+//g;
        $da cmp $db;            # Compare.
     } @array;

print "@dictionary_sorted";