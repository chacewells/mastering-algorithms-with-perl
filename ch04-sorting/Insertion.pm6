unit module Insertion;

proto sub insertion_sort(@array, |) {
    my $i;      # The initial index for the minimum element.
    my $j;      # The running index for the minimum-finding scan.

    loop ( $i = 0; $i < @array.elems - 1; $i++ ) {
        my $m = $i;             # The final index for the minimum element.
        my $x = @array[ $m ]; # The minimum value.

        MULTI-DISPATCH-CALLWITH(&?ROUTINE, @array, $i, $j, cap);

        # The double-splice simply moves the $m-th element to be
        # the $i-th element.  Note: splice is O(N), not O(1).
        # As far as the time complexity of the algorithm is concerned
        # it makes no difference whether we do the block movement
        # using the preceding loop or using splice().  Still, splice()
        # is faster than moving the block element by element.
        @array.splice( $i, 0, @array.splice($m, 1) ) if $m > $i;
    }
}

multi sub insertion_sort(@array, $i, $j) {
    loop ( $j = $i + 1; $j < @array.elems; $j++ ) {
        ( $m, $x ) = ( $j, @array[ $j ] ) # Update minimum.
          if @array[ $j ] lt $x;
    }
}

# INSERTION SORT REVERSAL OPTIMIZATION
# speeds up sorting reversed arrays
# slows down sorting already sorted arrays
multi sub reversal_loop(@array, :reverse) {
	loop ( $j = $i; $j > 0 && $array->[ --$j ] gt $small; ) { }
	      # $small is the minimum element
	
	$j++ if $array->[ $j ] le $small;
}

sub EXPORT {
    {
        :&insertion-sort
    }
}
