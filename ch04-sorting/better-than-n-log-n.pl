# It is possible to do better than O(N log N). Doing so requires knowledge about the keys before the sort begins.

# RADIX SORT
# there are many versions
# all use internal structure of the keys to speed up the sort
# a radix is a unit of structure, like the base of a number system
# keys must have same # of bits so the algorithm could inspect the key
package Sort::Radix::Straight;
sub radix_sort {
    my $array = shift;

    my $from = $array;
    my $to;

    # All lengths expected equal.
    for ( my $i = length $array->[ 0 ] - 1; $i >= 0; $i-- ) {
        # A new sorting bin.
        $to = [ ];
        foreach my $card ( @$from ) {
            # Stability is essential, so we use push().
            push @{ $to->[ ord( substr $card, $i ) ] }, $card;
        }

        # Concatenate the bins.

        $from = [ map { @{ $_ || [ ] } } @$to ];
    }

    # Now copy the elements back into the original array.

    @$array = @$from;
}

# COUNTING SORT
# counts elements by counting them in a sparse array
package Sort::Counting;

sub counting_sort {
    my ($array, $max) = @_; # All @$array elements must be 0..$max.
    my @counter = (0) x ($max+1);
    foreach my $elem ( @$array ) { $counter[ $elem ]++ }
    return map { ( $_ ) x $count[ $_ ] } 0..$max;
}

# BUCKET SORT
# for uniformly distributed numerical data
# groups data into buckets, then insertion or bubble sorts
# the buckets
package Sort::Bucket;

use constant BUCKET_SIZE => 10;

sub bucket_sort {
    my ($array, $min, $max) = @_;
    my $N = @$array or return;

    my $range    = $max - $min;
    my $N_BUCKET = $N / BUCKET_SIZE;
    # Create the buckets.
    my @bucket = map [], 1 .. $N_BUCKET;

    # Fill the buckets.
    for ( my $i = 0; $i < $N; $i++ ) {
        my $bucket = $N_BUCKET * (($array->[ $i ] - $min)/$range);
        push @{ $bucket[ $bucket ] }, $array->[ $i ];
    }
    # Sort inside the buckets.
    for ( my $i = 0; $i < $N_BUCKET; $i++ ) {
        insertion_sort( $bucket[ $i ] );
    }

    # Concatenate the buckets.
    @{ $array } = map { @{ $_ } } @bucket;
}

# QUICK BUCKET SORT
# Hybrid of quick sort and bubble sort
# Partition until partition size is narrower than a predefined
# threshold, then bubble sort the whole array (since it is
# now mostly sorted, which is what bubble sort is good for)
package Sort::QuickBubble;
sub qbsort_quick;
sub partitionMo3;

sub qbsort {
    qbsort_quick $_[0], 0, $#{ $_[0] }, defined $_[1] ? $_[1] : 10;
    bubblesmart   $_[0]; # Use the variant that's fast for almost sorted data.
}

# The first half of the quickbubblesort: quicksort.
# A completely normal quicksort (using median-of-three)
# except that only partitions larger than $width are sorted.

sub qbsort_quick {
    my ( $array, $first, $last, $width ) = @_;
    my @stack = ( $first, $last );
    do {
        if ( $last - $first > $width ) {
            my ( $last_of_first, $first_of_last ) =
                partitionMo3( $array, $first, $last );

            if ( $first_of_last - $first > $last - $last_of_first ) {
                push @stack, $first, $first_of_last;
                $first = $last_of_first;
            } else {
                push @stack, $last_of_first, $last;
                $last = $first_of_last;
            }
        } else { # Pop.
            ( $first, $last ) = splice @stack, -2, 2;
        }
    } while @stack;
}

sub partitionMo3 {
    my ( $array, $first, $last ) = @_;
use integer;
my $middle = int(( $first + $last ) / 2);
# Shuffle the first, middle, and last so that the median
    # is at the middle.
@$array[ $first, $middle ] = @$array[ $middle, $first ]
        if ( $$array[ $first ] gt $$array[ $middle ] );
@$array[ $first, $last ] = @$array[ $last, $first ]
        if ( $$array[ $first ] gt $$array[ $last ] );
@$array[ $middle, $last ] = @$array[ $last, $middle ]
        if ( $$array[ $middle ] lt $$array[ $last ] );
my $i = $first;
    my $j = $last - 1;
    my $pivot = $$array[ $last ];
# Now do the partitioning around the median.
SCAN: {
        do {
            # $first <= $i <= $j <= $last - 1
            # Point 1.
# Move $i as far as possible.
            while ( $$array[ $i ] le $pivot ) {
                $i++;
                last SCAN if $j < $i;
            }

            # Move $j as far as possible.
            while ( $$array[ $j ] ge $pivot ) {
                $j--;
                last SCAN if $j < $i;
            }

            # $i and $j did not cross over,
            # swap a low and a high value.
            @$array[ $j, $i ] = @$array[ $i, $j ];
        } while ( --$j >= ++$i );
    }
    # $first - 1 <= $j <= $i <= $last
    # Point 2.

    # Swap the pivot with the first larger element
    # (if there is one).
    if( $i < $last ) {
        @$array[ $last, $i ] = @$array[ $i, $last ];
        ++$i;
    }

    # Point 3.

    return ( $i, $j );   # The new bounds exclude the middle.
}
