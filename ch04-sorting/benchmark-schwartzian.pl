use Benchmark;

srand; # Randomize.
       # NOTE: for Perls < 5.004 
       # use srand(time + $$ + ($$ << 15)) for better results

# Generate a nice random input array.
@array = reverse 'aaa'..'zaz';

# Mutate the @array.
for ( @array ) {
     if (rand() < 0.5) {   # Randomly capitalize.
         $_ = ucfirst;
     }
     if (rand() < 0.25) {  # Randomly insert underscores.
         substr($_, rand(length), 0)= '_';
     }
     if (rand() < 0.333) { # Randomly double.
         $_ .= $_;
     }
     if (rand() < 0.333) { # Randomly mirror double.
         $_ .= reverse $_;
     }
     if (rand() > 1/length) { # Randomly delete characters.
         substr($_, rand(length), rand(length)) = '';
     }
}

# timethese() comes from Benchmark.

timethese(10, {
    'ST' =>
    '@sorted =
        map { $_->[0] }
            sort { $a->[1] cmp $b->[1] }
                map { # The dictionarization.
                  my $d = lc;
                  $d =~ s/[\W_]+//g;
                  [ $_, $d ]
                }
                @array',
   'nonST' =>
   '@sorted =
       sort { my ($da, $db) = ( lc( $a ), lc( $b ) );
              $da =~ s/[\W_]+//g;
              $db =~ s/[\W_]+//g;
              $da cmp $db;
            }
            @array'
    });
