# SELECTION SORT
# for every element, scan every element after that
# for the smallest available element
# swap after the scan
# O(N^2)
sub selection_sort {
    my $array = shift;

    my $i;      # The starting index of a minimum-finding scan.
    my $j;      # The running  index of a minimum-finding scan.

    for ( $i = 0; $i < $#$array ; $i++ ) {
        my $m = $i;             # The index of the minimum element.
        my $x = $array->[ $m ]; # The minimum value.

        for ( $j = $i + 1; $j < @$array; $j++ ) {
            ( $m, $x ) = ( $j, $array->[ $j ] ) # Update minimum.
              if $array->[ $j ] lt $x;
        }

        # Swap if needed.
        @$array[ $m, $i ] = @$array[ $i, $m ] unless $m == $i;
    }
}

# BUBBLE SORT
# for i, where i is the last index, down to the first index
# for j, where j is the second index, up to i
# compare e[j-1] to e[j] and swap if e[j-1] gt e[j]
# the largest value will bubble up first;
# the second largest will bubble up second, and so on..
# NOTE: for almost fully-sorted data, this is the fastest of all
# O(N^2) worst case
sub bubblesort {
    my $array = shift;

    my $i;              # The initial index for the bubbling scan.
    my $j;              # The running index for the bubbling scan.
    my $ncomp = 0;      # The number of comparisons.
    my $nswap = 0;      # The number of swaps.

    for ( $i = $#$array; $i; $i-- ) {
        for ( $j = 1; $j <= $i; $j++ ) {
            $ncomp++;
            # Swap if needed.
            if ( $array->[ $j - 1 ] gt $array->[ $j ] ) {
                @$array[ $j, $j - 1 ] = @$array[ $j - 1, $j ];
                $nswap++;
            }
        }
    }
    print "bubblesort:  ", scalar @$array,
          " elements, $ncomp comparisons, $nswap swaps\n";
}

# BUBBLE SMART
# a modification of bubble sort that reduces the left and right bounds
# to minimize the number of scans it must make over the list
sub bubblesmart {
    my $array = shift;
    my $start = 0;        # The start index of the bubbling scan.
    my $ncomp = 0;        # The number of comparisons.
    my $nswap = 0;        # The number of swaps.

    my $i = $#$array;

    while ( 1 ) {
        my $new_start;    # The new start index of the bubbling scan.
        my $new_end = 0;  # The new end index of the bubbling scan.

        for ( my $j = $start || 1; $j <= $i; $j++ ) {
            $ncomp++;
            if ( $array->[ $j - 1 ] gt $array->[ $j ] ) {
                @$array[ $j, $j - 1 ] = @$array[ $j - 1, $j ];
                $nswap++;
                $new_end   = $j - 1;
                $new_start = $j - 1 unless defined $new_start;
            }
        }
        last unless defined $new_start; # No swaps: we're done.
        $i     = $new_end;
        $start = $new_start;
    }
    print "bubblesmart: ", scalar @$array,
          " elements, $ncomp comparisons, $nswap swaps\n";
}

sub compare_bubble_reg_v_smart {
    my(@a,@b,@c,@d);
	@a = "a".."z";
	
	# Reverse sorted, both equally bad.
	@b = reverse @a;
	
	# Few inserts at the end.
	@c = ( @a, "a".."e" );
	
	# Random shuffle.
	srand();
	foreach ( @d = @a ) {
	    my $i = rand @a;
	    ( $_, $d[ $i ] ) = ( $d[ $i ], $_ );
	}
	
	my @label = qw(Sorted Reverse Append Random);
	my %label;
	@label{\@a, \@b, \@c, \@d} = 0..3;
	foreach my $var ( \@a, \@b, \@c, \@d ) {
	    print $label[$label{$var}], "\n";
	    bubblesort  [ @$var ];
	    bubblesmart [ @$var ];
	}

}

# INSERTION SORT
# basically a reverse bubble-sort
# moves next-smallest to its proper place
# shifts the rest up
# (linked list maybe useful for something like this to cut down on shifting cost?)
sub insertion_sort {
    my $array = shift;

    my $i;      # The initial index for the minimum element.
    my $j;      # The running index for the minimum-finding scan.

    for ( $i = 0; $i < $#$array; $i++ ) {
        my $m = $i;             # The final index for the minimum element.
        my $x = $array->[ $m ]; # The minimum value.

        for ( $j = $i + 1; $j < @$array; $j++ ) {
            ( $m, $x ) = ( $j, $array->[ $j ] ) # Update minimum.
              if $array->[ $j ] lt $x;
        }

        # The double-splice simply moves the $m-th element to be
        # the $i-th element.  Note: splice is O(N), not O(1).
        # As far as the time complexity of the algorithm is concerned
        # it makes no difference whether we do the block movement
        # using the preceding loop or using splice().  Still, splice()
        # is faster than moving the block element by element.
        splice @$array, $i, 0, splice @$array, $m, 1 if $m > $i;
    }
}

# INSERTION SORT REVERSAL OPTIMIZATION
# speeds up sorting reversed arrays
# slows down sorting already sorted arrays
sub reversal_loop {
	for ( $j = $i;
	      $j > 0 && $array->[ --$j ] gt $small; ) { }
	      # $small is the minimum element
	
	$j++ if $array->[ $j ] le $small;
}

# INSERTION MERGE
# hybrid situation
# say you have a big sorted array and a small array to merge into it
# sort the small array, then merge into larger to cut merging costs
# this routine assumes both are sorted
sub insertion_merge {
    my ( $large, $small ) = @_;

    my $merge;  # The merged result.
    my $i;      # The index to @merge.
    my $l;      # The index to @$large.
    my $s;      # The index to @$small.

    $#$merge = @$large + @$small - 1; # Pre-extend.

    for ( ($i, $l, $s) = (0, 0, 0); $i < @$merge; $i++ ) {
        $merge->[ $i ] =
          $l < @$large &&
            ( $s == @$small || $large->[ $l ] < $small->[ $s ] ) ?
              $large->[ $l++ ] :
              $small->[ $s++ ] ;
    }

    return $merge;
}

# SHELL SORT
# similar to insertion sort, but
# swaps elements over a distance
# and shrinks the portion of the array its working with
# when the part before and after are fully sorted
sub shellsort {
    my $array = shift;

    my $i;              # The initial index for the bubbling scan.
    my $j;              # The running index for the bubbling scan.
    my $shell;          # The shell size.
    my $ncomp = 0;      # The number of comparisons.
    my $nswap = 0;      # The number of swaps.

    for ( $shell = 1; $shell < @$array; $shell = 2 * $shell + 1 ) {
        # Do nothing here, just let the shell grow.
    }

    do {
        $shell = int( ( $shell - 1 ) / 2 );
        for ( $i = $shell; $i < @$array; $i++ ) {
            for ( $j = $i - $shell;
                  $j >= 0 && ++$ncomp &&
                    $array->[ $j ] gt $array->[ $j + $shell ];
                  $j -= $shell ) {
                @$array[ $j, $j + $shell ] = @$array[ $j + $shell, $j ];
                $nswap++;
            }
        }
    } while $shell > 1;
    print "shellsort:   ", scalar @$array,
          " elements, $ncomp comparisons, $nswap swaps\n";
}
