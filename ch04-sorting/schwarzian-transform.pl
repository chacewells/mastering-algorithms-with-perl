use locale;

# Fill @array here.

# map to [ original, dict ]
# compare dict a and dict b
# map back to original
@dictionary_sorted =
       map { $_->[0] }
           sort { $a->[1] cmp $b->[1] }
             map {
                   my $d = lc;          # Convert into lowercase.
                   $d =~ s/[\W_]+//g;   # Remove nonalphanumerics.
                   [ $_, $d ]           # [original, transformed]
                 }
           @array;

# in this case: use strings (faster than arrayref)
@dictionary_sorted =
    map { /^\w* (.*)/ }
       sort
          map {
               my $d = lc;          # Convert into lowercase.
               $d =~ s/[\W_]+//g;   # Remove nonalphanumerics.
               "$d $_"              # Concatenate new and original words.
              }
        @array;
