use strict;
use feature 'say';
use constant {
    NEXT => 0,
    VAL => 1,
};
sub newlist; # predeclare list to play with
sub repeatlist; # predeclare list to play with
sub dummylist; # repeatlist using a dummy head

{ # inserting an item
my $list = newlist;

# pred is the predecessor to the item we're inserting
my $pred = $list->[NEXT][NEXT]; # the 3rd item

# pred's next is updated with a new item pointing to pred's previous NEXT
# O(1) constant time
$pred->[NEXT] = [ $pred->[NEXT], 49 ];
}

{ # deleting an item at the head
my $list = newlist;
my $list = $list->[NEXT];
}

{ # remembering the value
my $list = newlist;
my $val = $list->[VAL];
$list = $list->[NEXT];
}

{ # deleting in the middle of the list
my $list = newlist;
my $pred = $list->[NEXT];
$pred->[NEXT] = $pred->[NEXT][NEXT];
}

{ # middle of list, but remember value
my $list = newlist;
my $pred = $list->[NEXT];
my $val = $pred->[NEXT][VAL];
$pred->[NEXT] = $pred->[NEXT][NEXT];
}

{ # safely deleting an element if we don't know it exists
my $list = newlist;

# meaning val is undefined if the list is empty, otherwise the value of the deleted item
my $val;
if ($val = $list) {
    $val = $list->[VAL];
    $list = $list->[NEXT];
}
say "SAFE DELETE: $val removed";
}

{ # processing a list as we remove elements
my $list = newlist;

# the while loop provides context
# so we can be sure the node removals are always safe
while ($list) {
    my $val = $list->[VAL];
    $list = $list->[NEXT];

    # process $val
    say "WHILE LOOP: $val removed";
}
}

{ # searching a list
my $list = newlist;
my $target = 25;
my $elem;
for ($elem = $list; $elem; $elem = $elem->[NEXT] ) {
    if ($elem->[VAL] == $target) {
        say "SEARCH: found ", $elem->[VAL];
        last;
    }
}

unless ($elem) {
    say "SEARCH: didn't find $target";
}
}

{ # search a list while keeping reference to the last one
  # useful if you need to remove items while traversing
my $list = repeatlist;
my ($target, $delete_count) = (4, 0); # how many items removed

# pred is always a ref ref
# elem is pred dereffed and terminates the loop when undefined
# pred is updated with the next node ref
for (my ($pred,$elem) = \$list; $elem = $$pred; $pred = \$elem->[NEXT]) {
    if ( $elem->[VAL] == $target ) {
        if ( $elem = $$pred = $elem->[NEXT] ) {
            ++$delete_count;
            redo;
        }
        last;
    }
}

print "deleted [$target] $delete_count times\n";
}

{
my $list = dummylist;
my ($target,$delete_count) = (10,0);

# Search for an element on a list that has a dummy header.
for ( my ($pred,$elem) = $list; $elem = $pred->[NEXT]; $pred = $elem) {
    ++$delete_count, next if $elem->[VAL] == $target;
}

say "deleted [$target] $delete_count times";
}

{ # reverse the contents of a list
my $list = newlist;
my $list_reverse = sub {
    my $old = shift;
    my $new = undef;

    while (my $cur = $old) {
        $old = $old->[NEXT];
        $cur->[NEXT] = $new;
        $new = $cur;
    }

    return $new;
};

local $_ = $list;
print "LIST REVERSE [before]: ";
print $_->[VAL].(($_ = $_->[NEXT]) ? ',' : "\n") while $_;

$list = $list_reverse->($list); # 5,4,3,2,1
print "LIST REVERSE [after]: ";
$_ = $list;
print $_->[VAL].(($_ = $_->[NEXT]) ? ',' : "\n") while $_;
}

sub newlist {
    my $list = undef;
    my $tail = \$list;
    foreach (1..5) {
        my $node = [ undef, $_ ** 2 ];
        $$tail = $node; # update the last item's next to point to node
        $tail = \$node->[NEXT]; # tail is pointer to null
    }

    return $list;
}

sub repeatlist { # build a list that repeats 1 .. 10
    my $list = undef;
    my $tail = \$list;
    for (my $i = 1; $i <= 100; ++$i) {
        my $node = [ undef, 1 + ($i % 10) ];
        $$tail = $node;
        $tail = \$node->[NEXT];
    }

    return $list;
}

# build a list whose tail element is a "dummy" element
=head1 Advantages
Storing a reference to an entire element is less confusing than a reference to the link field
Like using the ref field, this removes the special cases for:
* tail tracking
* search for a predecessor operations 
=cut
sub dummylist {
    my $list = [ undef, undef ];
    $list->[VAL] = $list; # initially the dummy is also the tail

    # add elements to the end of the repeat list
    for ( my $i = 1; $i <= 100; ++$i ) {
        $list->[VAL] = $list->[VAL][NEXT] = [ undef, 1 + ($i % 10) ];
    }

    return $list;
}
