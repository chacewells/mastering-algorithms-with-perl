package double;

# $node = double->new( $val );
#
# Create a new double element with value $val.
sub new {
    my $class = shift;
    $class = ref($class) || $class;
    my $self = { val=>shift };
    bless $self, $class;
    return $self->_link_to( $self );
}

# $elem1->_link_to( $elem2 )
#
# Join this node to another, return self.
# (This is for internal use only, it doesn't not care whether
# the elements linked are linked into any sort of correct
# list order.)
sub _link_to {
    my ( $node, $next ) = @_;

    $node->next( $next );
    return $next->prev( $node );
}

sub destroy {
    my $node = shift;
    while( $node ) {
        my $next = $node->next;
        $node->prev(undef);
        $node->next(undef);
        $node = $next;
    }
}

# $cur = $node->next
# $new = $node->next( $new )
#
#    Get next link, or set (and return) a new value in next link.
sub next {
    my $node = shift;
    return @_ ? ($node->{next} = shift) : $node->{next};
}

# $cur = $node->prev
# $new = $node->prev( $new )
#
#    Get prev link, or set (and return) a new value in prev link.
sub prev {
    my $node = shift;
    return @_ ? ($node->{prev} = shift) : $node->{prev};
}

# $elem1->append( $elem2 )
# $elem->append( $head )
#
# Insert the list headed by another node (or by a list) after
# this node, return self.
sub append {
    my ( $node, $add ) = @_;
    if ( $add = $add->content ) {
        $add->prev->_link_to( $node->next );
        $node->_link_to( $add );
    }
    return $node;
}

# Insert before this node, return self.
sub prepend {
    my ( $node, $add ) = @_;
    if ( $add = $add->content ) {
        $node->prev->_link_to( $add->next );
        $add->_link_to( $node );
    }
    return $node;
}


# Content of a node is itself unchanged
# (needed because for a list head, content must remove all of
# the elements from the list and return them, leaving the head
# containing an empty list).
sub content {
    return shift;
}

# Remove one or more nodes from their current list and return the
# first of them.
# The caller must ensure that there is still some reference
# to the remaining other elements.
sub remove {
    my $first = shift;
    my $last = shift || $first;

    # Remove it from the old list.
    $first->prev->_link_to( $last->next );

    # Make the extracted nodes a closed circle.
    $last->_link_to( $first );
    return $first;
}
1;
