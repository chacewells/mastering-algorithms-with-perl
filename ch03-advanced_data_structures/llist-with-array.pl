use v5.18;
use constant NEXT => 0;
use constant VAL => 1;

package Backward
{
    # building a list backward
    our $list = undef;
    foreach (reverse 1..5) {
        $list = [ $list, $_ ** 2 ];
    }

    # switching 2 elements
    my ($four, $nine, $sixteen);
    $four = $list->[NEXT];
    $nine = $four->[NEXT];
    $sixteen = $nine->[NEXT];

    $nine->[NEXT] = $sixteen->[NEXT]; # twenty-five
    $sixteen->[NEXT] = $nine;
    $four->[NEXT] = $sixteen; # 9 and 16 just traded places
}

package ForwardWithIf
{
    our $list = $tail = undef;
    foreach (1..5) {
        my $node = [ undef, $_ * $_ ];
        if ( not defined $tail ) {
            # first one is special - it becomes both the head and the tail
            $list = $tail = $node;
        } else {
            # subsequent elements are added after the previous tail
            $tail->[NEXT] = $node;
            # and advance the tail pointer to the new tail
            $tail = $node;
        }
    }
}

package ForwardNoIf
{
    # tricky business. study further
    our $list = undef;
    our $tail = \$list; # apparently tail starts as a ref to list
    foreach (1..5) {
        my $node = [ undef, $_ * $_ ];
        $$tail = $node; # update as ref to new node
        $tail = $node->[NEXT]; # never assigning node->[NEXT] ?
    }
}
