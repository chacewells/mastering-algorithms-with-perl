use double; use double_head;

my $sq = double_head::->new( "squares" );
my $cu = double_head::->new( "cubes" );
my $three;

for( $i = 0; $i < 5; ++$i ) {
    my $new = double->new( $i*$i );
    $sq->append($new);
    $sq->ldump;
    $new = double->new( $i*$i*$i );
    $three = $new if $i == 3;
    $cu->append($new);
    $cu->ldump;
}

# $sq is a list of squares from 0*0 .. 5*5
# $cu is a list of cubes from 0*0*0 .. 5*5*5

# Move the first cube to the end of the squares list.
$sq->append($cu->first->remove);

# Move 3*3*3 from the cubes list to the front of the squares list.
$sq->prepend($cu->first->remove( $three ) );

$sq->ldump;
$cu->ldump;
