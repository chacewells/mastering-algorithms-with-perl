package double_head;

sub new {
    my $class = shift;
    my $info = shift;
    my $dummy = double->new;

    bless [ $dummy, $info ], $class;
}
sub DESTROY {
    my $self = shift;
    my $dummy = $self->[0];

    $dummy->destroy;
}


# Prepend to the dummy header to append to the list.
sub append {
    my $self = shift;
    $self->[0]->prepend( shift );
    return $self;
}

# Append to the dummy header to prepend to the list.
sub prepend {
    my $self = shift;
    $self->[0]->append( shift );
    return $self;
}

# Return a reference to the first element.
sub first {
    my $self = shift;
    my $dummy = $self->[0];
    my $first = $dummy->next;

    return $first == $dummy ? undef : $first;
}

# Return a reference to the last element.
sub last {
    my $self = shift;
    my $dummy = $self->[0];
    my $last = $dummy->prev;

    return $last == $dummy ? undef : $last;
}

# When an append or prepend operation uses this list,
# give it all of the elements (and remove them from this list
# since they are going to be added to the other list).
sub content {
    my $self = shift;
    my $dummy = $self->[0];
    my $first = $dummy->next;
    return undef if $first eq $dummy;
    $dummy->remove;
    return $first;
}

sub ldump {
    my $self = shift;
    my $start = $self->[0];
    my $cur = $start->next;
    print "list($self->[1]) [";
    my $sep = "";

    while( $cur ne $start ) {
        print $sep, $cur->{val};
        $sep = ",";
        $cur = $cur->next;
    }
    print "]\n";
}

1;
