use v5.10;
use strict;
use Data::Dumper;

MAIN: {
    # The tree starts out empty.
    my $tree = undef;
    my $node;

    foreach ( 1..8 ) {
        ($tree, $node) = bal_tree_add( $tree, $_ * $_ );
    }

    ($tree, $node) = bal_tree_del( $tree, 7*7 );

    print Dumper $tree, $node;
}

# Usage:
# ($link, $node) = basic_tree_find( \$tree, $target, $cmp )
#
# Search the tree \$tree for $target.  The optional $cmp
# argument specifies an alternative comparison routine
# (called as $cmp->( $item1, $item2 ) to be used instead
# of the default numeric comparison.  It should return a
# value consistent with the <=> or cmp operators.
#
# Return two items:
#
#    1. a reference to the link that points to the node
#       (if it was found) or to the place where it should
#       go (if it was not found)
#
#    2. the node itself (or undef if it doesn't exist)

sub basic_tree_find {
    my ($tree_link, $target, $cmp) = @_;
    my $node;
    
    # $tree_link is the next pointer to be followed.
    # It will be undef if we reach the bottom of the tree.
    while ( $node = $$tree_link ) {
        local $^W = 0;      # no warnings, we expect undef values

        my $relation = ( defined $cmp
                    ? $cmp->( $target, $node->{val} )
                    : $target <=> $node->{val} );
    # If we found it, return the answer.
    return ($tree_link, $node) if $relation == 0;

    # Nope - prepare to descend further - decide which way we go.
        $tree_link = $relation > 0 ? \$node->{left} : \$node->{right};
    }

    # We fell off the bottom, so the element isn't there, but we
    # tell caller where to create a new element (if desired).
    return ($tree_link, undef);
}


# $node = basic_tree_add( \$tree, $target, $cmp );
#
# If there is not already a node in the tree \$tree that
# has the value $target, create one.  Return the new or
# previously existing node.  The third argument is an
# optional comparison routine and is simply passed on to
# basic_tree_find.

sub basic_tree_add {
    my ($tree_link, $target, $cmp) = @_;
    my $found;

    ($tree_link, $found) = basic_tree_find( $tree_link, $target, $cmp );

    unless ($found) {
        $found = {
            left  => undef,
            right => undef,
            val   => $target
        };
        $$tree_link = $found;
    }

    return $found;
}


# $val = basic_tree_del( \$tree, $target[, $cmp ] );
#
# Find the element of \$tree that has the value $val
# and remove it from the tree.  Return the value, or
# return undef if there was no appropriate element
# on the tree.

sub basic_tree_del {
    my ($tree_link, $target, $cmp) = @_;
    my $found;

    ($tree_link, $found) = basic_tree_find ( $tree_link, $target, $cmp );

    return undef unless $found;

    # $tree_link has to be made to point to any children of $found:
    #  if there are no children, make it null
    #  if there is only one child, it can just take the place
    #    of $found
    #  But, if there are two children, they have to be merged somehow
    #    to fit in the one reference.
    #
    if ( ! defined $found->{left} ) {
        $$tree_link = $found->{right};
    } elsif ( ! defined $found->{right} ) {
        $$tree_link = $found->{left};
    } else {
        MERGE_SOMEHOW( $tree_link, $found );
    }

    return $found->{val};
}


# OPTION 1: SIMPLE
# MERGE_SOMEHOW
#
# Make $tree_link point to both $found->{left} and $found->{right}.

# Attach $found->{left} to the leftmost child of $found->{right}
# and then attach $found->{right} to $$tree_link.
sub MERGE_SOMEHOW {
    my ($tree_link, $found) = @_;
    my $left_of_right = $found->{right};
    my $next_left;

    $left_of_right = $next_left
        while $next_left = $left_of_right->{left};

    $left_of_right->{left} = $found->{left};

    $$tree_link = $found->{right};
}

# traverse( $tree, $func )
#
# Traverse $tree in order, calling $func() for each element.
#    in turn

sub traverse {
    my $tree = shift or return;   # skip undef pointers
    my $func = shift;

    traverse( $tree->{left}, $func );
    &$func( $tree );
    traverse( $tree->{right}, $func );
}

# $node = bal_tree_find( $tree, $val[, $cmp ] )
#
# Search $tree looking for a node that has the value $val.
# If provided, $cmp compares values instead of <=>.
#
# the return value:
#     $node points to the node that has value $val
#        or undef if no node has that value

sub bal_tree_find {
    my ($tree, $val, $cmp) = @_;
    my $result;

    while ( $tree ) {
        my $relation = defined $cmp
            ? $cmp->( $tree->{val}, $val )
            : $tree->{val} <=> $val;

        # Stop when the desired node is found.
        return $tree if $relation == 0;

        # Go down to the correct subtree.
        $tree = $relation < 0 ? $tree->{left} : $tree->{right};
    }

    # The desired node doesn't exist.
    return undef;
}


# ($tree, $node) = bal_tree_add( $tree, $val, $cmp )
#
# Search $tree looking for a node that has the value $val;
#    add it if it does not already exist.
# If provided, $cmp compares values instead of <=>.
#
# the return values:
#     $tree points to the (possibly new or changed) subtree that
#        has resulted from the add operation
#     $node points to the (possibly new) node that contains $val

sub bal_tree_add {
    my ($tree, $val, $cmp) = @_;
    my $result;

    # Return a new leaf if we fell off the bottom.
    unless ( $tree ) {
        $result = {
                left   => undef,
                right  => undef,
                val    => $val,
                height => 1
            };
        return( $result, $result );
    }

    my $relation = defined $cmp
        ? $cmp->( $tree->{val}, $val )
        : $tree->{val} <=> $val;

    # Stop when the desired node is found.
    return ( $tree, $tree ) if $relation == 0;

    # Add to the correct subtree.
    if ( $relation < 0 ) {
        ($tree->{left}, $result)  =
            bal_tree_add ( $tree->{left}, $val, $cmp );
    } else {
        ($tree->{right}, $result) =
            bal_tree_add ( $tree->{right}, $val, $cmp );
    }

    # Make sure that this level is balanced, return the
    #    (possibly changed) top and the (possibly new) selected node.
    return ( balance_tree( $tree ), $result );
}

# ($tree, $node) = bal_tree_del( $tree, $val, $cmp )
    #
    # Search $tree looking for a node that has the value $val,
    #    and delete it if it exists.
    # If provided, $cmp compares values instead of <=>.
    #
    # the return values:
    #     $tree points to the (possibly empty or changed) subtree that
    #        has resulted from the delete operation
    #     if found, $node points to the node that contains $val
    #     if not found, $node is undef

sub bal_tree_del {
    # An empty (sub)tree does not contain the target.
    my $tree = shift or return (undef,undef);

    my ($val, $cmp) = @_;
    my $node;

    my $relation = defined $cmp
        ? $cmp->($val, $tree->{val})
        : $val <=> $tree->{val};

    if ( $relation != 0 ) {
        # Not this node, go down the tree.
        if ( $relation < 0 ) {
            ($tree->{left},$node) =
                bal_tree_del( $tree->{left}, $val, $cmp );
        } else {
            ($tree->{right},$node) =
                bal_tree_del( $tree->{right}, $val, $cmp );
        }

        # No balancing required if it wasn't found.
        return ($tree,undef) unless $node;
    } else {
        # Must delete this node.  Remember it to return it,
        $node = $tree;

        # but splice the rest of the tree back together first
        $tree = bal_tree_join( $tree->{left}, $tree->{right} );# and make the deleted node forget its children (precaution
        # in case the caller tries to use the node).
        $node->{left} = $node->{right} = undef;
    }

    # Make sure that this level is balanced, return the
    #    (possibly changed) top and (possibly undef) selected node.
    return ( balance_tree($tree), $node );
}


# $tree = balance_tree( $tree )

sub balance_tree {
    # An empty tree is balanced already.
    my $tree = shift or return undef;

    # An empty link is height 0.
    my $lh = defined $tree->{left} && $tree->{left}{height};
    my $rh = defined $tree->{right} && $tree->{right}{height};

    # Rebalance if needed, return the (possibly changed) root.
    if ( $lh > 1+$rh ) {
        return swing_right( $tree );
    } elsif ( $lh+1 < $rh ) {
        return swing_left( $tree );
    } else {
        # Tree is either perfectly balanced or off by one.
        # Just fix its height.
        set_height( $tree );
        return $tree;
    }
}


# set_height( $tree )
sub set_height {
    my $tree = shift;
    my $p;
    # get heights, an undef node is height 0
    my $lh = defined ( $p = $tree->{left}  ) && $p->{height};
    my $rh = defined ( $p = $tree->{right} ) && $p->{height};
    $tree->{height} = $lh < $rh ? $rh+1 : $lh+1;
}


# $tree = swing_left( $tree )
#
# change       t        to          r         or         rl
#             / \                  / \                 /    \
#            l   r                t   rr              t      r
#               / \              / \                 / \    / \
#              rl  rr           l   rl              l  rll rlr rr
#             / \                  / \
#           rll rlr              rll rlr
#
# t and r must both exist.
# The second form is used if height of rl is greater than height of rr
# (since the first form would then lead to the height of t at least 2
# more than the height of rr).
#
# Changing to the second form is done in two steps, with first a
# move_right(r) and then a move_left(t), so it goes:
#
# change       t        to          t     and then to    rl
#             / \                  / \                 /    \
#            l   r                l   rl              t      r
#               / \                  / \             / \    / \
#              rl  rr              rll  r           l  rll rlr rr
#             / \                      / \
#           rll rlr                  rlr  rr

sub swing_left {
    my $tree = shift;
    my $r = $tree->{right};         # must exist
    my $rl = $r->{left};            # might exist
    my $rr = $r->{right};           # might exist
    my $l = $tree->{left};          # might exist

    # get heights, an undef node has height 0
    my $lh = $l && $l->{height};
    my $rlh = $rl && $rl->{height};
    my $rrh = $rr && $rr->{height};

    if ( $rlh > $rrh ) {
        $tree->{right} = move_right( $r );
    }

    return move_left( $tree );
}

# and the opposite swing

sub swing_right {
    my $tree = shift;
    my $l = $tree->{left};          # must exist
    my $lr = $l->{right};           # might exist
    my $ll = $l->{left};            # might exist
    my $r = $tree->{right};         # might exist

    # get heights, an undef node has height 0
    my $rh = $r && $r->{height};
    my $lrh = $lr && $lr->{height};
    my $llh = $ll && $ll->{height};

    if ( $lrh > $llh ) {
        $tree->{left} = move_left( $l );
    }

    return move_right( $tree );
}


# $tree = move_left( $tree )
#
# change       t        to          r
#             / \                  / \
#            l   r                t   rr
#               / \              / \
#              rl  rr           l   rl
#
# caller has determined that t and r both exist
#   (l can be undef, so can one of rl and rr)

sub move_left {
    my $tree = shift;
    my $r = $tree->{right};
    my $rl = $r->{left};

    $tree->{right} = $rl;
    $r->{left} = $tree;
    set_height( $tree );
    set_height( $r );
    return $r;
}

# $tree = move_right( $tree )
#
# opposite change from move_left

sub move_right {
    my $tree = shift;
    my $l = $tree->{left};
    my $lr = $l->{right};

    $tree->{left} = $lr;
    $l->{right} = $tree;
    set_height( $tree );
    set_height( $l );
    return $l;
}
