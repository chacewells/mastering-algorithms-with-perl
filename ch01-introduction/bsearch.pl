#!/usr/bin/perl
#
# bsearch - search for a word in a list of alphabetically ordered words
# Usage: bsearch word filename

$word = shift;                          # Assign first argument to $word
chomp( @array = <> );                   # Read in newline-delimited words,
                                        #     truncating the newlines

($word, @array) = map lc, ($word, @array); # Convert all to lowercase
$index = binary_search(\@array, $word);    # Invoke our algorithm

if (defined $index) { print "$word occurs at position $index.\n" }
else                { print "$word doesn't occur.\n" }

sub binary_search {
    my ($array, $word) = @_;
    my $low = 0;
    my $high = @$array - 1;

    while ( $low <= $high ) {
        my $try = int( ($low+$high) / 2 );
        $low  = $try+1, next if $array->[$try] lt $word;
        $high = $try-1, next if $array->[$try] gt $word;
        return $try;
    }
    return;
}