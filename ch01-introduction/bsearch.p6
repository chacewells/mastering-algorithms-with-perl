#!/usr/bin/env perl6
#
# bsearch - search for a word in a list of alphabetically ordered words
# Usage: bsearch word filename

sub MAIN($word is copy, $file) {
    $word.=lc;
    my @array = $file.IO.slurp.words».lc;
    my $index = binary_search(@array, $word);
    if $index.defined { say "$word occurs at position $index" }
    else              { say "$word doesn't occur." }
}

sub binary_search (@array, $word) {
    my $low = 0;
    my $high = @array.elems - 1;

    while $low <=> $high < 0 {
        my $try = ( ($low+$high) / 2 ).Int;
        $low  = $try+1, next if @array[$try] lt $word;
        $high = $try-1, next if @array[$try] gt $word;
        return $try;
    }
    return;
}
